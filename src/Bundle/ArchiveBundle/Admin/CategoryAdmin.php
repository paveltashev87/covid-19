<?php

namespace App\Bundle\ArchiveBundle\Admin;

use App\Bundle\TreeBundle\Admin\AbstractTreeAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\AdminType;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class CategoryAdmin extends AbstractTreeAdmin
{
     protected $translationDomain = 'SonataAdminBundle';


    protected function configureRoutes(RouteCollection $collection)
    {

        $collection
            ->add('showImages', $this->getRouterIdParameter());
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            // ->add('id')
            ->add('name')
            // ->add('path')
            // ->add('lft')
            // ->add('lvl')
            // ->add('rgt')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            // ->add('id')
            ->add('name')
            // ->add('path')
            // ->add('lft')
            // ->add('lvl')
            // ->add('rgt')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                    'showImages' => [
                        'template' => 'AppBundleArchiveBundle:CRUD:list__action_clone.html.twig',
                    ],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            // ->add('id')
            ->add('name')
            // ->add('path')
            // ->add('lft')
            // ->add('lvl')
            // ->add('rgt')
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            // ->add('id')
            ->add('name')
            ->end()
            // ->add('path')
            ->with('Images')
            ->add('images')
            // ->add('lft')
            // ->add('lvl')
            // ->add('rgt')
        ;
    }
}
