<?php

namespace App\Bundle\ArchiveBundle\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;
use Sonata\MediaBundle\Provider\Metadata;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Sonata\DoctrineORMAdminBundle\Filter\ModelAutocompleteFilter;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Sonata\FormatterBundle\Form\Type\SimpleFormatterType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

use Sonata\MediaBundle\Provider\Pool;

class ImageAdmin extends AbstractAdmin
{

    private $pool;
    protected $translationDomain = 'SonataAdminBundle';


    public function __construct(string $code, string $class, string $baseControllerName, Pool $pool)
    {
        $this->pool = $pool;
        parent::__construct($code, $class, $baseControllerName);
    }

    public function getObjectMetadata($object): Metadata
    {
        $media = $object->getImage();
        $cacheManager = $this->getConfigurationPool()->getContainer()->get('liip_imagine.cache.manager');
        $url  = $cacheManager->getBrowserPath($media, 'default_big');

        return new Metadata($object->getKeywords(), $object->getKeywords(), $url);
    }


    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('title')
            ->add('comment')
            ->add('titleE')
            ->add('commentE')
            ->add('keywords')
            ->add('code')
//            ->add('preview')
//            ->add('image')
//            ->add('linked')
//            ->add('volume')
//            ->add('hdd')
            ->add('category', ModelAutocompleteFilter::class, [], null, [
                'property'=>'name',
            ]);
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('image', null, array('template' => 'AppBundleArchiveBundle:ImageAdmin:list_image.html.twig'))
            ->add('id')
            ->add('title')
            // ->add('comment')
            // ->add('titleE')
            // ->add('commentE')
//            ->add('keywords')
            // ->add('image')
            // ->add('linked')
            // ->add('volume')
            // ->add('hdd')
            ->add('category')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
//            ->add('id')
            ->add('title')
//            ->add('comment', TextType::class, [ 'required' => false ])
            ->add('titleE', TextType::class, [ 'required' => false ])
//            ->add('commentE', TextType::class, [ 'required' => false ])
            ->add('keywords', TextType::class, [ 'required' => true ])
            ->add('code', ChoiceType::class, [
                'choices'  => [
                    'bg' => 'bg',
                    'world'     => 'world',
                ]
            ])
            ->add('text', SimpleFormatterType::class, [
                'format' => 'text',
                'required' => false
            ])
            ->add('textEn', SimpleFormatterType::class, [
                'format' => 'text',
                'required' => false
            ])
            ->add('image', FileType::class, [
                    'required' => false,
                    'data_class'  => null
                ])
//            ->add('linked')
//            ->add('volume')
//            ->add('hdd')
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('title')
            ->add('comment')
            ->add('titleE')
            ->add('commentE')
            ->add('keywords')
            ->add('preview')
            ->add('image')
//            ->add('linked')
//            ->add('volume')
//            ->add('hdd')
        ;
    }
}
