<?php

namespace App\Bundle\ArchiveBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class NewsAdmin extends AbstractAdmin
{
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('newsCode')
            ->add('title')
            ->add('titleTop')
            ->add('annotation')
            ->add('tags')
            ->add('content')
            ->add('isActive')
            ->add('isHead')
            ->add('isTop')
            ->add('isTicker')
            ->add('createdOn')
            ->add('updatedOn')
            ->add('createdBy')
            ->add('updatedBy')
            ->add('source')
            ->add('headImage')
            ->add('video')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('newsCode')
            ->add('title')
            ->add('titleTop')
            ->add('annotation')
            ->add('tags')
            ->add('content')
            ->add('isActive')
            ->add('isHead')
            ->add('isTop')
            ->add('isTicker')
            ->add('createdOn')
            ->add('updatedOn')
            ->add('createdBy')
            ->add('updatedBy')
            ->add('source')
            ->add('headImage')
            ->add('video')
            ->add('_action', null, [
                'actions' => [
                    'show' => [],
                    'edit' => [],
                    'delete' => [],
                ],
            ])
        ;
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('id')
            ->add('newsCode')
            ->add('title')
            ->add('titleTop')
            ->add('annotation')
            ->add('tags')
            ->add('content')
            ->add('isActive')
            ->add('isHead')
            ->add('isTop')
            ->add('isTicker')
            ->add('createdOn')
            ->add('updatedOn')
            ->add('createdBy')
            ->add('updatedBy')
            ->add('source')
            ->add('headImage')
            ->add('video')
        ;
    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $showMapper
            ->add('id')
            ->add('newsCode')
            ->add('title')
            ->add('titleTop')
            ->add('annotation')
            ->add('tags')
            ->add('content')
            ->add('isActive')
            ->add('isHead')
            ->add('isTop')
            ->add('isTicker')
            ->add('createdOn')
            ->add('updatedOn')
            ->add('createdBy')
            ->add('updatedBy')
            ->add('source')
            ->add('headImage')
            ->add('video')
        ;
    }
}
