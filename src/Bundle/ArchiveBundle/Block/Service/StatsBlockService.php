<?php

namespace App\Bundle\ArchiveBundle\Block\Service;
use Doctrine\ORM\EntityManagerInterface;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\CoreBundle\Validator\ErrorElement;
use Sonata\BlockBundle\Block\Service\AbstractBlockService;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\BlockBundle\Model\BlockInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bundle\TwigBundle\TwigEngine;
use Symfony\Component\DependencyInjection\ContainerInterface as Container;

class StatsBlockService extends AbstractBlockService
{
    private $entityManager;
    private $container;

    public function __construct(string $serviceId, TwigEngine $templating, EntityManagerInterface $entityManager, Container $container)
    {
        $this->entityManager = $entityManager;
        $this->container = $container;
        parent::__construct($serviceId, $templating);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'Stats Block';
    }

    /**
     * {@inheritdoc}
     */
    public function configureSettings(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'entity' => 'App\Bundle\ArchiveBundle\Entity\Image',
            'repository_method' => 'countAllImages',
            'title' => 'count_all_images',
            'css_class' => 'green',
            'arrow' => 'red',
            'position' => 'desc',
            'icon' => 'fa-users',
            'template' => 'AppBundleArchiveBundle:Block:block_stats.html.twig',
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function buildEditForm(FormMapper $formMapper, BlockInterface $block)
    {
        $formMapper->add('settings', 'sonata_type_immutable_array', array(
            'keys' => array(
                array('entity', 'text', array('required' => false)),
                array('repository_method', 'text', array('required' => false)),
                array('title', 'text', array('required' => false)),
                array('css_class', 'text', array('required' => false)),
                array('arrow', 'text', array('required' => false)),
                array('position', 'text',array('required' => false)),
                array('icon', 'text', array('required' => false)),
            ),
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function validateBlock(ErrorElement $errorElement, BlockInterface $block)
    {
        $errorElement
            ->with('settings[entity]')
                ->assertNotNull(array())
                ->assertNotBlank()
            ->end()
            ->with('settings[repository_method]')
                ->assertNotNull(array())
                ->assertNotBlank()
            ->end()
            ->with('settings[title]')
                ->assertNotNull(array())
                ->assertNotBlank()
                ->assertMaxLength(array('limit' => 50))
            ->end()
            ->with('settings[css_class]')
                ->assertNotNull(array())
                ->assertNotBlank()
            ->end()
            ->with('settings[arrow]')
                ->assertNotNull(array())
                ->assertNotBlank()
            ->end()
            ->with('settings[position]')
                ->assertNotNull(array())
                ->assertNotBlank()
            ->end()
            ->with('settings[icon]')
                ->assertNotNull(array())
                ->assertNotBlank()
            ->end();
    }

    /**
     * {@inheritdoc}
     */
    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $settings = $blockContext->getSettings();


        $entity = $settings['entity'];
        $method = $settings['repository_method'];

        $rows = $this->entityManager->getRepository($entity)->$method();

        return $this->templating->renderResponse($blockContext->getTemplate(), array(
            'count'     => $rows,
            'block'     => $blockContext->getBlock(),
            'settings'  => $settings,
        ), $response);
    }
}
