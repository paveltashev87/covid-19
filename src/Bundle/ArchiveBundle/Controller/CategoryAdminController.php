<?php

namespace App\Bundle\ArchiveBundle\Controller;

use App\Bundle\ArchiveBundle\Entity\Category;
use App\Bundle\TreeBundle\Controller\TreeAdminController;
use Sonata\AdminBundle\Controller\CRUDController;
use Symfony\Component\HttpFoundation\RedirectResponse;

class CategoryAdminController extends TreeAdminController
{
    /**
     * @param $id
     * @link CRUDController
     * @return RedirectResponse
     */
    public function showImagesAction($id)
    {
        $object = $this->admin->getSubject();
        if (!$object) {
            throw new NotFoundHttpException(sprintf('unable to find the object with id: %s', $id));
        }

        $this->addFlash('sonata_flash_success', 'All images for '.$object);
        return new RedirectResponse('/admin/app/archive/image/list?filter[category][value]='.$id);
    }
}
