<?php

namespace App\Bundle\ArchiveBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Bundle\ArchiveBundle\Util\RequestProcessor;


class DefaultController extends Controller
{

    /**
     * @Route("/")
     */
    public function indexAction()
    {
        $images = $this->getDoctrine()->getRepository('AppBundleArchiveBundle:Image')->getLatestAllImages();
        $this->container->get('monolog.logger.db')->info('index', [
            'page' => 'index'
        ]);
        return $this->render('AppBundleArchiveBundle:Default:index.html.twig', [ 'images' => $images]);
    }



    public function galleryAction(Request $request)
    {
        $page  = ((!$request->get('page')) ? 1 : $request->get('page'));

        $images = $this->getDoctrine()->getRepository('AppBundleArchiveBundle:Image')->getAllImages($page);
        $totalPostsReturned = $images->getIterator()->count();
        $totalPosts = $images->count();
        $iterator = $images->getIterator();

        $limit = 6;
        $maxPages = ceil($this->getDoctrine()->getRepository('AppBundleArchiveBundle:Image')->getAllImages()->count() / $limit);
        $thisPage = $page;

        $this->container->get('monolog.logger.db')->info('gallery_page', [
            'page' => 'gallery'
        ]);

        return $this->render('AppBundleArchiveBundle:Default:gallery.html.twig', [
            'images' => $images,
            'maxPages'=> $maxPages,
            'thisPage'=>$page,
        ]);
    }

    public function getImageDetails(Request $request )
    {
        $id  = ((!$request->get('id')) ? 1 : $request->get('id'));
        $image = $this->getDoctrine()->getRepository('AppBundleArchiveBundle:Image')->find($id);


        /*Бутони за навигация на снимка*/
        $previous = $this->getDoctrine()->getRepository('AppBundleArchiveBundle:Image')->getPreviousImage($id);
        $next = $this->getDoctrine()->getRepository('AppBundleArchiveBundle:Image')->getNextImage($id);

        $this->container->get('monolog.logger.db')->info('gallery_image', [
            'gallery_image' => $id
        ]);

        return $this->render('AppBundleArchiveBundle:Default:image.html.twig', [
            'image' => $image,
            'previous' => $previous,
            'next'  => $next
        ]);
    }



    public function newsAction(Request $request)
    {
        $page  = ((!$request->get('page')) ? 1 : $request->get('page'));

        $news = $this->getDoctrine()->getRepository('AppBundleArchiveBundle:News')->getAllNews($page);
        $totalPostsReturned = $news->getIterator()->count();
        $totalPosts = $news->count();
        $iterator = $news->getIterator();

        $limit = 10;
        $maxPages = ceil($this->getDoctrine()->getRepository('AppBundleArchiveBundle:News')->getAllNews()->count() / $limit);
        $thisPage = $page;

        return $this->render('AppBundleArchiveBundle:Default:news.html.twig', [
            'news' => $news,
            'maxPages'=> $maxPages,
            'thisPage'=>$page,
        ]);
    }
}
