<?php

namespace App\Bundle\ArchiveBundle\Controller;
use Sonata\AdminBundle\Controller\CRUDController;

class StatsCRUDController extends CRUDController
{
    public function listAction()
    {
        return $this->render('AppBundleArchiveBundle:CRUD:stats.html.twig');
    }
}


//https://medium.com/@kunicmarko20/sonata-admin-custom-page-61b99d4bd9d4