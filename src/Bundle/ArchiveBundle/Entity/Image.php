<?php

namespace App\Bundle\ArchiveBundle\Entity;

use Gedmo\Mapping\Annotation as Gedmo;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;
use function GuzzleHttp\Psr7\str;

/**
 * Image
 *
 * @ORM\Table(name="images")
 * @Gedmo\Loggable
 * @ORM\Entity(repositoryClass="App\Bundle\ArchiveBundle\Repository\ImageRepository")
 */
class Image
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

   /**
    * @var datetime
    *
    * @ORM\Column(name="createdAt", type="datetime")
    */
   private $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=1024, nullable=true)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="string", length=2048, nullable=true)
     */
    private $comment;

    /**
     * @var string
     *
     * @ORM\Column(name="title_e", type="string", length=1024, nullable=true)
     */
    private $titleE;

    /**
     * @var string
     *
     * @ORM\Column(name="comment_e", type="string", length=2048, nullable=true)
     */
    private $commentE;

    /**
     * @var string
     * @Gedmo\Versioned
     * @ORM\Column(name="keywords", type="string", length=2048, nullable=true)
     */
    private $keywords;

   /**
    * @var string
    *
    * @ORM\Column(name="text", type="text", nullable=true)
    */
   private $text;

    /**
     * @var string
     *
     * @ORM\Column(name="text_en", type="text", nullable=true)
     */
    private $textEn;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255)
     */
    private $image;


    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=25)
     */
    private $code;

    /**
     * @ORM\ManyToMany(targetEntity="App\Bundle\ArchiveBundle\Entity\Category", mappedBy="categoryImages")
     */
    private $category;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }


   /**
    * Set dateNN.
    *
    * @param datetime $createdAt
    *
    * @return Image
    */
   public function setCreatedAt($createdAt)
   {
       $this->createdAt = $createdAt;

       return $this;
   }


   /**
    * Get dateNN.
    *
    * @return datetime
    */
   public function getCreatedAt()
   {
       return $this->createdAt;
   }


    /**
     * Set title.
     *
     * @param string $title
     *
     * @return Image
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set comment.
     *
     * @param string $comment
     *
     * @return Image
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment.
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set titleE.
     *
     * @param string $titleE
     *
     * @return Image
     */
    public function setTitleE($titleE)
    {
        $this->titleE = $titleE;

        return $this;
    }

    /**
     * Get titleE.
     *
     * @return string
     */
    public function getTitleE()
    {
        return $this->titleE;
    }

    /**
     * Set commentE.
     *
     * @param string $commentE
     *
     * @return Image
     */
    public function setCommentE($commentE)
    {
        $this->commentE = $commentE;

        return $this;
    }

    /**
     * Get commentE.
     *
     * @return string
     */
    public function getCommentE()
    {
        return $this->commentE;
    }

    /**
     * Set keywords.
     *
     * @param string $keywords
     *
     * @return Image
     */
    public function setKeywords($keywords)
    {
        $this->keywords = $keywords;

        return $this;
    }

    /**
     * Get keywords.
     *
     * @return string
     */
    public function getKeywords()
    {
        return $this->keywords;
    }



   /**
    * Set text.
    *
    * @param  string $text
    *
    * @return Image
    */
   public function setText($text)
   {
       $this->text = $text;

       return $this;
   }

   /**
    * Get text.
    *
    * @return string
    */
   public function getText()
   {
       return $this->text;
   }


    /**
     * Set text.
     *
     * @param  string $textEn
     *
     * @return Image
     */
    public function setTextEn($textEn)
    {
        $this->textEn = $textEn;

        return $this;
    }

    /**
     * Get $textEn.
     *
     * @return string
     */
    public function getTextEn()
    {
        return $this->textEn;
    }

    /**
     * Set image.
     *
     * @param string $image
     *
     * @return Image
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image.
     *
     * @return string
     */
    public function getImage()
    {
        return $this->image;
    }


    /**
     * Set code.
     *
     * @param string $code
     *
     * @return Image
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    public function __toString()
    {
        return (string) $this->getTitle();
    }

    public function __construct()
    {
        $this->createdAt = new \DateTime();
    }

}
