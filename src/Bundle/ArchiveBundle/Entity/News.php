<?php

namespace App\Bundle\ArchiveBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * News
 *
 * @ORM\Table(name="news")
 * @ORM\Entity(repositoryClass="App\Bundle\ArchiveBundle\Repository\NewsRepository")
 */
class News
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;



    /**
     * @ORM\ManyToOne(targetEntity="App\Bundle\ArchiveBundle\Entity\AlbumState", inversedBy="current_state")
     * @ORM\JoinColumn(name="lock_state_id", referencedColumnName="id")
     */



    /**
     * @ORM\ManyToMany(targetEntity="App\Bundle\ArchiveBundle\Entity\NewsCategory", inversedBy="newsCode")
     * @ORM\Column(name="news_code", type="string", length=10)
     */
    private $newsCode;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=512)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="title_top", type="string", length=255, nullable=true)
     */
    private $titleTop;

    /**
     * @var string
     *
     * @ORM\Column(name="annotation", type="string", length=10000, nullable=true)
     */
    private $annotation;

    /**
     * @var string
     *
     * @ORM\Column(name="tags", type="string", length=512, nullable=true)
     */
    private $tags;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text", nullable=true)
     */
    private $content;

    /**
     * @var int
     *
     * @ORM\Column(name="is_active", type="integer")
     */
    private $isActive;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_head", type="boolean")
     */
    private $isHead;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_top", type="boolean")
     */
    private $isTop;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_ticker", type="boolean")
     */
    private $isTicker;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_on", type="datetime")
     */
    private $createdOn;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_on", type="datetime", nullable=true)
     */
    private $updatedOn;

    /**
     * @var int|null
     *
     * @ORM\Column(name="created_by", type="integer", nullable=true)
     */
    private $createdBy;

    /**
     * @var int|null
     *
     * @ORM\Column(name="updated_by", type="integer", nullable=true)
     */
    private $updatedBy;

    /**
     * @var string
     *
     * @ORM\Column(name="source", type="string", length=255, nullable=true)
     */
    private $source;

    /**
     * @var int|null
     *
     * @ORM\Column(name="head_image", type="integer", nullable=true, nullable=true)
     */
    private $headImage;

    /**
     * @var int
     *
     * @ORM\Column(name="video", type="integer", nullable=true)
     */
    private $video;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set newsCode.
     *
     * @param string $newsCode
     *
     * @return News
     */
    public function setNewsCode($newsCode)
    {
        $this->newsCode = $newsCode;

        return $this;
    }

    /**
     * Get newsCode.
     *
     * @return string
     */
    public function getNewsCode()
    {
        return $this->newsCode;
    }

    /**
     * Set title.
     *
     * @param string $title
     *
     * @return News
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title.
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set titleTop.
     *
     * @param string $titleTop
     *
     * @return News
     */
    public function setTitleTop($titleTop)
    {
        $this->titleTop = $titleTop;

        return $this;
    }

    /**
     * Get titleTop.
     *
     * @return string
     */
    public function getTitleTop()
    {
        return $this->titleTop;
    }

    /**
     * Set annotation.
     *
     * @param string $annotation
     *
     * @return News
     */
    public function setAnnotation($annotation)
    {
        $this->annotation = $annotation;

        return $this;
    }

    /**
     * Get annotation.
     *
     * @return string
     */
    public function getAnnotation()
    {
        return $this->annotation;
    }

    /**
     * Set tags.
     *
     * @param string $tags
     *
     * @return News
     */
    public function setTags($tags)
    {
        $this->tags = $tags;

        return $this;
    }

    /**
     * Get tags.
     *
     * @return string
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Set content.
     *
     * @param string $content
     *
     * @return News
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content.
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set isActive.
     *
     * @param int $isActive
     *
     * @return News
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive.
     *
     * @return int
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set isHead.
     *
     * @param bool $isHead
     *
     * @return News
     */
    public function setIsHead($isHead)
    {
        $this->isHead = $isHead;

        return $this;
    }

    /**
     * Get isHead.
     *
     * @return bool
     */
    public function getIsHead()
    {
        return $this->isHead;
    }

    /**
     * Set isTop.
     *
     * @param bool $isTop
     *
     * @return News
     */
    public function setIsTop($isTop)
    {
        $this->isTop = $isTop;

        return $this;
    }

    /**
     * Get isTop.
     *
     * @return bool
     */
    public function getIsTop()
    {
        return $this->isTop;
    }

    /**
     * Set isTicker.
     *
     * @param bool $isTicker
     *
     * @return News
     */
    public function setIsTicker($isTicker)
    {
        $this->isTicker = $isTicker;

        return $this;
    }

    /**
     * Get isTicker.
     *
     * @return bool
     */
    public function getIsTicker()
    {
        return $this->isTicker;
    }

    /**
     * Set createdOn.
     *
     * @param \DateTime $createdOn
     *
     * @return News
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * Get createdOn.
     *
     * @return \DateTime
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * Set updatedOn.
     *
     * @param \DateTime $updatedOn
     *
     * @return News
     */
    public function setUpdatedOn($updatedOn)
    {
        $this->updatedOn = $updatedOn;

        return $this;
    }

    /**
     * Get updatedOn.
     *
     * @return \DateTime
     */
    public function getUpdatedOn()
    {
        return $this->updatedOn;
    }

    /**
     * Set createdBy.
     *
     * @param int|null $createdBy
     *
     * @return News
     */
    public function setCreatedBy($createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return int|null
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy.
     *
     * @param int|null $updatedBy
     *
     * @return News
     */
    public function setUpdatedBy($updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy.
     *
     * @return int|null
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }

    /**
     * Set source.
     *
     * @param string $source
     *
     * @return News
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source.
     *
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set headImage.
     *
     * @param int|null $headImage
     *
     * @return News
     */
    public function setHeadImage($headImage = null)
    {
        $this->headImage = $headImage;

        return $this;
    }

    /**
     * Get headImage.
     *
     * @return int|null
     */
    public function getHeadImage()
    {
        return $this->headImage;
    }

    /**
     * Set video.
     *
     * @param int $video
     *
     * @return News
     */
    public function setVideo($video)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get video.
     *
     * @return int
     */
    public function getVideo()
    {
        return $this->video;
    }
}
