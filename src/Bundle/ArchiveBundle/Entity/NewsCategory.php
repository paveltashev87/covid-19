<?php

namespace App\Bundle\ArchiveBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * NewsCategory
 *
 * @ORM\Table(name="news_category")
 * @ORM\Entity(repositoryClass="App\Bundle\ArchiveBundle\Repository\NewsCategoryRepository")
 */
class NewsCategory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=10)
     */
    private $code;


    /**
     * @ORM\OneToMany(targetEntity="App\Bundle\ArchiveBundle\Entity\News", mappedBy="news_code")
     */
    private $newsCode;


    /**
     * @var string
     *
     * @ORM\Column(name="lang", type="string", length=10)
     */
    private $lang;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_paid", type="boolean")
     */
    private $isPaid;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_client", type="boolean")
     */
    private $isClient;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_public", type="boolean")
     */
    private $isPublic;

    /**
     * @var string
     *
     * @ORM\Column(name="shown_in_menu", type="boolean")
     */
    private $shownInMenu;

    /**
     * @var bool
     *
     * @ORM\Column(name="dont_delete", type="boolean")
     */
    private $dontDelete;

    /**
     * @var bool
     *
     * @ORM\Column(name="use_in_mobile", type="boolean")
     */
    private $useInMobile;

    /**
     * @var int
     *
     * @ORM\Column(name="display_order", type="integer")
     */
    private $displayOrder;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_on", type="datetime")
     */
    private $createdOn;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_on", type="datetime")
     */
    private $updatedOn;

    /**
     * @var int
     *
     * @ORM\Column(name="created_by", type="integer", nullable=true )
     */
    private $createdBy;

    /**
     * @var int
     *
     * @ORM\Column(name="updated_by", type="integer", nullable=true)
     */
    private $updatedBy;


    /**
     * @var int
     *
     * @Gedmo\SortablePosition
     * @ORM\Column(name="position", type="integer", nullable=true)
     */
    private $position;


    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set code.
     *
     * @param string $code
     *
     * @return NewsCategory
     */
    public function setCode($code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Get code.
     *
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Set lang.
     *
     * @param string $lang
     *
     * @return NewsCategory
     */
    public function setLang($lang)
    {
        $this->lang = $lang;

        return $this;
    }

    /**
     * Get lang.
     *
     * @return string
     */
    public function getLang()
    {
        return $this->lang;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return NewsCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set isPaid.
     *
     * @param bool $isPaid
     *
     * @return NewsCategory
     */
    public function setIsPaid($isPaid)
    {
        $this->isPaid = $isPaid;

        return $this;
    }

    /**
     * Get isPaid.
     *
     * @return bool
     */
    public function getIsPaid()
    {
        return $this->isPaid;
    }

    /**
     * Set isClient.
     *
     * @param bool $isClient
     *
     * @return NewsCategory
     */
    public function setIsClient($isClient)
    {
        $this->isClient = $isClient;

        return $this;
    }

    /**
     * Get isClient.
     *
     * @return bool
     */
    public function getIsClient()
    {
        return $this->isClient;
    }

    /**
     * Set isPublic.
     *
     * @param bool $isPublic
     *
     * @return NewsCategory
     */
    public function setIsPublic($isPublic)
    {
        $this->isPublic = $isPublic;

        return $this;
    }

    /**
     * Get isPublic.
     *
     * @return bool
     */
    public function getIsPublic()
    {
        return $this->isPublic;
    }

    /**
     * Set shownInMenu.
     *
     * @param bool $shownInMenu
     *
     * @return NewsCategory
     */
    public function setShownInMenu($shownInMenu)
    {
        $this->shownInMenu = $shownInMenu;

        return $this;
    }

    /**
     * Get shownInMenu.
     *
     * @return bool
     */
    public function getShownInMenu()
    {
        return $this->shownInMenu;
    }

    /**
     * Set dontDelete.
     *
     * @param bool $dontDelete
     *
     * @return NewsCategory
     */
    public function setDontDelete($dontDelete)
    {
        $this->dontDelete = $dontDelete;

        return $this;
    }

    /**
     * Get dontDelete.
     *
     * @return bool
     */
    public function getDontDelete()
    {
        return $this->dontDelete;
    }

    /**
     * Set useInMobile.
     *
     * @param bool $useInMobile
     *
     * @return NewsCategory
     */
    public function setUseInMobile($useInMobile)
    {
        $this->useInMobile = $useInMobile;

        return $this;
    }

    /**
     * Get useInMobile.
     *
     * @return bool
     */
    public function getUseInMobile()
    {
        return $this->useInMobile;
    }

    /**
     * Set displayOrder.
     *
     * @param int $displayOrder
     *
     * @return NewsCategory
     */
    public function setDisplayOrder($displayOrder)
    {
        $this->displayOrder = $displayOrder;

        return $this;
    }

    /**
     * Get displayOrder.
     *
     * @return int
     */
    public function getDisplayOrder()
    {
        return $this->displayOrder;
    }

    /**
     * Set createdOn.
     *
     * @param \DateTime $createdOn
     *
     * @return NewsCategory
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * Get createdOn.
     *
     * @return \DateTime
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * Set updatedOn.
     *
     * @param \DateTime $updatedOn
     *
     * @return NewsCategory
     */
    public function setUpdatedOn($updatedOn)
    {
        $this->updatedOn = $updatedOn;

        return $this;
    }

    /**
     * Get updatedOn.
     *
     * @return \DateTime
     */
    public function getUpdatedOn()
    {
        return $this->updatedOn;
    }

    /**
     * Set createdBy.
     *
     * @param int $createdBy
     *
     * @return NewsCategory
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy.
     *
     * @return int
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set updatedBy.
     *
     * @param int $updatedBy
     *
     * @return NewsCategory
     */
    public function setUpdatedBy($updatedBy)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy.
     *
     * @return int
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }



    /**
     * Set position.
     *
     * @param int $position
     *
     * @return NewsCategory
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position.
     *
     * @return int
     */
    public function getPosition()
    {
        return $this->updatedBy;
    }



    public function __toString()
    {
        return (string) $this->name;
    }

}
