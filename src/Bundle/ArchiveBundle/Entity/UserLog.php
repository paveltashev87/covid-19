<?php

namespace App\Bundle\ArchiveBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Bundle\ArchiveBundle\Repository\UserLogRepository")
 * @ORM\Table(name="user_log")
 * @ORM\HasLifecycleCallbacks
 */
class UserLog
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="message", type="text")
     */
    private $message;


//    /**
//     * @ORM\ManyToOne(targetEntity="App\Bundle\ArchiveBundle\Entity\User", inversedBy="user")
//     * @ORM\JoinColumn(name="user", referencedColumnName="id")
//     */
//    private $user;

    /**
     * @ORM\Column(name="context", type="array")
     */
    private $context;

    /**
     * @ORM\Column(name="level", type="smallint")
     */
    private $level;

    /**
     * @ORM\Column(name="level_name", type="string", length=50)
     */
    private $levelName;

    /**
     * @ORM\Column(name="user_ip", type="string", length=45)
     */
    private $userIp;

    /**
     * @ORM\Column(name="uri", type="string", length=150)
     */
    private $uri;

    /**
     * @ORM\Column(name="method", type="string", length=50)
     */
    private $method;

    /**
     * @ORM\Column(name="extra", type="array")
     */
    private $extra;

    /**
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\PrePersist
     */
    public function onPrePersist()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set message.
     *
     * @param string $message
     *
     * @return UserLog
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message.
     *
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Set context.
     *
     * @param array $context
     *
     * @return UserLog
     */
    public function setContext($context)
    {
        $this->context = $context;

        return $this;
    }

    /**
     * Get context.
     *
     * @return array
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * Set level.
     *
     * @param int $level
     *
     * @return UserLog
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level.
     *
     * @return int
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set levelName.
     *
     * @param string $levelName
     *
     * @return UserLog
     */
    public function setLevelName($levelName)
    {
        $this->levelName = $levelName;

        return $this;
    }

    /**
     * Get levelName.
     *
     * @return string
     */
    public function getLevelName()
    {
        return $this->levelName;
    }

    /**
     * Set extra.
     *
     * @param array $extra
     *
     * @return UserLog
     */
    public function setExtra($extra)
    {
        $this->extra = $extra;

        return $this;
    }

    /**
     * Get extra.
     *
     * @return array
     */
    public function getExtra()
    {
        return $this->extra;
    }

    /**
     * Set createdAt.
     *
     * @param \DateTime $createdAt
     *
     * @return UserLog
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt.
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set method.
     *
     * @param string $method
     *
     * @return UserLog
     */
    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    /**
     * Get method.
     *
     * @return string
     */
    public function getMethod()
    {
        return $this->method;
    }

    /**
     * Set userIp.
     *
     * @param int $userIp
     *
     * @return UserLog
     */
    public function setUserIp($userIp)
    {
        $this->userIp = $userIp;

        return $this;
    }

    /**
     * Get userIp.
     *
     * @return int
     */
    public function getUserIp()
    {
        return $this->userIp;
    }

    /**
     * Set uri.
     *
     * @param string $uri
     *
     * @return UserLog
     */
    public function setUri($uri)
    {
        $this->uri = $uri;

        return $this;
    }

    /**
     * Get uri.
     *
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

//    /**
//     * Set user.
//     *
//     * @param \AppBundle\Entity\User|null $user
//     *
//     * @return UserLog
//     */
//    public function setUser(\AppBundle\Entity\User $user = null)
//    {
//        $this->user = $user;
//
//        return $this;
//    }
//
//    /**
//     * Get user.
//     *
//     * @return \AppBundle\Entity\User|null
//     */
//    public function getUser()
//    {
//        return $this->user;
//    }
}
