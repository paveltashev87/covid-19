<?php
namespace App\Bundle\ArchiveBundle\EventListener;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use App\Bundle\ArchiveBundle\Entity\Image;
use App\Bundle\ArchiveBundle\Service\FileUploader;

class UploadListener
{
    private $uploader;
    private $fileName;

    public function __construct(FileUploader $uploader)
    {
        $this->uploader = $uploader;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        $this->uploadFile($entity);
    }

    public function preUpdate(PreUpdateEventArgs $args)
    {
        // Тука взума проверката за форма на клас Image
        $entity = $args->getEntity();
        if (!$entity instanceof Image) {
            return;
        }
        $changes = $args->getEntityChangeSet();
        $previousFilename = null;

        // Проверка за смяна на името
        if(array_key_exists("image", $changes)){
            // Обновяваме старото име
            $previousFilename = $changes["image"][0];
        }

        // Ако няма картинка във формата
        if(is_null($entity->getImage())){
            // Оставяме старото име във базата
            $entity->setImage($previousFilename);
//            $entity->setPreview($previousFilename);

            //ако има картинка във формата
        }else{
            // ако предишен файл съществува
            if(!is_null($previousFilename)){
                $pathPreviousFile = $this->uploader->getTargetDir(). "/". $previousFilename;

                //махаме съществуващата
                if(file_exists($pathPreviousFile)){
                    unlink($pathPreviousFile);
                }
            }
            //тука се обръща към FileUploader-a
            $this->uploadFile($entity);
        }
    }

    private function uploadFile($entity)
    {
        //само за Image класа
        if (!$entity instanceof Image) {
            return;
        }
        $file = $entity->getImage();
        // само нов файл
        if ($file instanceof UploadedFile) {
            $fileName = $this->uploader->upload($file);
            $entity->setImage($fileName);
//            $entity->setPreview($fileName);
        }
    }
}
