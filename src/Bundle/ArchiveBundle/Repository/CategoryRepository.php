<?php

namespace App\Bundle\ArchiveBundle\Repository;

use Doctrine\DBAL\Query\QueryBuilder;

use Gedmo\Tree\Entity\Repository\NestedTreeRepository;

class CategoryRepository extends NestedTreeRepository
{
	public function countAllCategories()
	{
		$countAllCategories = $this->createQueryBuilder('c')
			->select('COUNT(c.id)')
			// ->where('bl.user = :user')
			// ->andWhere('DATE(bl.createdAt) = :date')
			// ->setParameter('date', $date->format('Y-m-d'))
			// ->setParameter('user', $user)
			->getQuery()->getSingleScalarResult();
		return $countAllCategories;
	}

}
