<?php

namespace App\Bundle\ArchiveBundle\Repository;

use Doctrine\DBAL\Query\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Twig\Node\Expression\Binary\LessEqualBinary;

class ImageRepository extends \Doctrine\ORM\EntityRepository
{
	public function countAllImages()
	{
		$countAllImages = $this->createQueryBuilder('i')
			->select('COUNT(i.id)')
			// ->where('bl.user = :user')
			// ->andWhere('DATE(bl.createdAt) = :date')
			// ->setParameter('date', $date->format('Y-m-d'))
			// ->setParameter('user', $user)
			->getQuery()->getSingleScalarResult();


		return $countAllImages;
	}


    public function getLatestAllImages()
    {
        $getLatestAllImages = $this->createQueryBuilder('i')
            ->orderBy('i.createdAt', 'DESC')
            ->setMaxResults(6)
            // ->select('COUNT(i.id)')
            // ->where('bl.user = :user')
            // ->andWhere('DATE(bl.createdAt) = :date')
            // ->setParameter('date', $date->format('Y-m-d'))
            // ->setParameter('user', $user)
            ->getQuery()->getResult();


        return $getLatestAllImages;
    }


    /**
     * Our new getAllPosts() method
     *
     * 1. Create & pass query to paginate method
     * 2. Paginate will return a `\Doctrine\ORM\Tools\Pagination\Paginator` object
     * 3. Return that object to the controller
     *
     * @param integer $currentPage The current page (passed from controller)
     *
     * @return \Doctrine\ORM\Tools\Pagination\Paginator
     */
    public function getAllImages($currentPage = 1)
    {
        $query = $this->createQueryBuilder('i')
            ->orderBy('i.createdAt', 'DESC')
            ->getQuery();
        $paginator = $this->paginate($query, $currentPage);

        return $paginator;
    }

    /**
     * Paginator Helper
     *
     * Pass through a query object, current page & limit
     * the offset is calculated from the page and limit
     * returns an `Paginator` instance, which you can call the following on:
     *
     *     $paginator->getIterator()->count() # Total fetched (ie: `5` posts)
     *     $paginator->count() # Count of ALL posts (ie: `20` posts)
     *     $paginator->getIterator() # ArrayIterator
     *
     * @param Doctrine\ORM\Query $dql   DQL Query Object
     * @param integer            $page  Current page (defaults to 1)
     * @param integer            $limit The total number per page (defaults to 5)
     *
     * @return \Doctrine\ORM\Tools\Pagination\Paginator
     */
    public function paginate($dql, $page = 1, $limit = 6)
    {
        $paginator = new Paginator($dql);

        $paginator->getQuery()
            ->setFirstResult($limit * ($page - 1)) // Offset
            ->setMaxResults($limit); // Limit

        return $paginator;
    }

    //Резултат за предишния вход
    public function getPreviousImage($id)
    {
        $previous = $this->createQueryBuilder('i')
            ->where('i.id < :id')
            ->orderBy('i.id', 'DESC')
            ->setParameter( 'id', $id)
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult();
        return $previous;
    }

    //Резултат за следващ вход
    public function getNextImage($id)
    {
        $next = $this->createQueryBuilder('i')
            ->where('i.id > :id')
            ->orderBy('i.id', 'ASC')
            ->setParameter( 'id', $id)
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult();
        return $next;
    }
}
