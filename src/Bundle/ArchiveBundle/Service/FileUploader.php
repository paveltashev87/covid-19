<?php

namespace App\Bundle\ArchiveBundle\Service;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUploader
{
    private $targetDir;

    public function __construct($targetDir)
    {
        $this->targetDir = $targetDir;
    }

    public function resizeImageSquare($file)
    {
        copy($file, $this->getTargetDir().'/thumbnail/'.basename($file));
        $new = imagecreatefromjpeg($this->getTargetDir().'/thumbnail/'.basename($file));

        $thumbnail = $this->getTargetDir().'/thumbnail/'.basename($file);

        $crop_width = imagesx($new);
        $crop_height = imagesy($new);

        $size = min($crop_width, $crop_height);


        if($crop_width >= $crop_height) {
            $newx= ($crop_width-$crop_height)/2;

            $im2 = imagecrop($new, ['x' => $newx, 'y' => 0, 'width' => $size, 'height' => $size]);
        }
        else {
            $newy= ($crop_height-$crop_width)/2;

            $im2 = imagecrop($new, ['x' => 0, 'y' => $newy, 'width' => $size, 'height' => $size]);
        }
        return imagejpeg($im2,$thumbnail,90);
    }

    public function upload(UploadedFile $file)
    {
        $fileName = md5(uniqid()).'.'.$file->guessExtension();

        $file->move($this->getTargetDir(), $fileName);
//        $this->resizeImageSquare($this->getTargetDir().'/'.$fileName);

        return $fileName;
    }

    public function getTargetDir()
    {
        return $this->targetDir;
    }
}