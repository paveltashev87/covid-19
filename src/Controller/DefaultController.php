<?php

namespace App\Controller;

//use Symfony\Bundle\FrameworkBundle\Controller\Controller;
//use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class DefaultController extends AbstractController
{

    public function indexAction()
    {
        $entityManager = $this->getDoctrine()->getManager();

        $images = $entityManager->getRepository('AppBundleArchiveBundle:Image')->getLatestAllImages();
        return $this->render('@AppBundleArchive/Default/index.html.twig', [
            'images' => $images
        ]);
    }
}
