<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;

final class LocaleController
{
    /**
     * @Route("/locale/{locale}", name="locale")
     * @param Request $request
     * @param string $locale
     * @return RedirectResponse
     */
    public function index(Request $request, string $locale): RedirectResponse
    {
        $request->getSession()->set('_locale', $locale);
        return new RedirectResponse($request->headers->get('referer', '/'));
    }
}

