<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use App\Application\Sonata\UserBundle\Entity\User;
use DateTime;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $data = new DateTime();
        $user = new User();
        $roles = ['a:1:{i:0;s:16:"ROLE_SUPER_ADMIN";}'];
        $user->setUsername('admin');
        $user->setUsernameCanonical('admin');
        $user->setEmail('admin@mail.localhost');
        $user->setEmailCanonical('admin@mail.localhost');
        $user->setEnabled(1);
        $user->setPassword("$2y$13$1iQx2OB5lXuWR/ynSIhcJeFUG9AocGa3msIOpFZ4/G0sTuZr5dYYO");
        $user->setCreatedAt($data);
        $user->setRoles($roles);
        $manager->persist($user);
        $manager->flush();
    }
}
